<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WifiProbe extends Model
{
    protected $table = 'wifiprobetest';

    protected $fillable = ['probeid','mmac','rate','lat','lon'];

    public function datas()
    {
        return $this->hasMany(WifiProbeData::class,'probeid','probeid');
    }

}
