<?php

namespace App\Http\Controllers;

use App\WifiProbe;
use App\WifiProbeData;
use Exception;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request)
    {

        try{


//        $data = '{"id":"0029c591","mmac":"5e:cf:7f:29:c5:91","rate":"1","time":"Tue Feb 21 08:13:31 2017","lat":"30.747988","lon":"103.973152" ,"data":
//[{"mac":"a4:56:02:61:7f:57","rssi":"-91","range":"91.5"},{"mac":"8c:a6:df:62:2d:3d","rssi":"-93","range":"108.5"},{"mac":"a4:56:02:71:be:b3 ","rssi":"-96","range":"140.1"},{"mac":"cc:34:29:97:4d:0d","rssi":"-95","range":"128.6"},{"mac":"44:33:4c:aa:71:82","rssi":"-94","range":"11 8.1"},{"mac":"b0:48:7a:5a:10:f8","rssi":"-86","range":"59.7"},{"mac":"a8:57:4e:9d:ca:d8","rssi":"-96","range":"140.1"},{"mac":"5e:cf:7f:93:3 d:0e","rssi":"-56","range":"4.6"},{"mac":"5e:cf:7f:93:3d:0f","rssi":"-58","range":"5.5"},{"mac":"5e:cf:7f:93:3d:10","rssi":"-63","range":"8. 4"},{"mac":"5e:cf:7f:93:3d:0b","rssi":"-68","range":"12.9"},{"mac":"5e:cf:7f:93:3d:0c","rssi":"-53","range":"3.5"},{"mac":"5e:cf:7f:93:3d:0d ","rssi":"-69","range":"14.0"},{"mac":"e4:f3:f5:24:2c:d8","rssi":"-89","range":"77.1"},{"mac":"14:cf:92:8a:8f:f0","rssi":"-96","range":"140. 1"}]}';
//        $data = $_POST['data'];
            $data=$request->get('data');
            //--解析 Json，获取对应的变量值
            $obj = json_decode($data,JSON_UNESCAPED_UNICODE);
            info($obj);
            $id = $obj['id'];
            $mmac = $obj['mmac'];
            $rate = $obj['rate'];
            $time = $obj['time'];

            $lat = $obj['lat'];
            $lon = $obj['lon'];

            $wifiprobe=new WifiProbe();
            $wifiprobe->probeid=$id;
            $wifiprobe->mmac=$mmac;
            $wifiprobe->rate=$rate;
            $wifiprobe->time=$time;
            $wifiprobe->lat=$lat;
            $wifiprobe->lon=$lon;
            $wifiprobe->save();

            $detail_data = $obj['data'];

            $i = 0;//循环变量
            //--得到 Json_list 数组长度
            $num = count($obj["data"]);
            //--遍历数组，将对应信息输出
            for ($i; $i < $num; $i++) {

                $wifiprobedata=new WifiProbeData();
                $wifiprobedata->probeid=$id;

                if(array_key_exists("mac",$detail_data[$i]))
                {
                    $mac = $detail_data[$i]["mac"];
                    $wifiprobedata->mac=$mac;
                }

                if(array_key_exists("rssi",$detail_data[$i]))
                {
                    $rssi = $detail_data[$i]["rssi"];
                    $wifiprobedata->rssi=$rssi;
                }

                if(array_key_exists("router",$detail_data[$i]))
                {
                    $router = $detail_data[$i]["router"];
                    $wifiprobedata->router=$router;
                }

                if(array_key_exists("range",$detail_data[$i]))
                {
                    $range = $detail_data[$i]["range"];
                    $wifiprobedata->range=$range;
                }

                if(array_key_exists("ts",$detail_data[$i]))
                {
                    $ts = $detail_data[$i]["ts"];
                    $wifiprobedata->ts=$ts;
                }

                if(array_key_exists("tmc",$detail_data[$i]))
                {
                    $tmc = $detail_data[$i]["tmc"];
                    $wifiprobedata->tmc=$tmc;
                }

                if(array_key_exists("tc",$detail_data[$i]))
                {
                    $tc = $detail_data[$i]["tc"];
                    $wifiprobedata->tc=$tc;
                }

                if(array_key_exists("ds",$detail_data[$i]))
                {
                    $ds = $detail_data[$i]["ds"];
                    $wifiprobedata->ds=$ds;
                }

                if(array_key_exists("essid1",$detail_data[$i]))
                {
                    $essid1 = $detail_data[$i]["essid1"];
                    $wifiprobedata->essid1=$essid1;
                }

                if(array_key_exists("essid2",$detail_data[$i]))
                {
                    $essid2 = $detail_data[$i]["essid2"];
                    $wifiprobedata->essid2=$essid2;
                }

                if(array_key_exists("essid3",$detail_data[$i]))
                {
                    $essid3 = $detail_data[$i]["essid3"];
                    $wifiprobedata->essid3=$essid3;
                }

                if(array_key_exists("essid4",$detail_data[$i]))
                {
                    $essid4 = $detail_data[$i]["essid4"];
                    $wifiprobedata->essid4=$essid4;
                }

                if(array_key_exists("essid5",$detail_data[$i]))
                {
                    $essid5 = $detail_data[$i]["essid5"];
                    $wifiprobedata->essid5=$essid5;
                }

                if(array_key_exists("essid6",$detail_data[$i]))
                {
                    $essid6 = $detail_data[$i]["essid6"];
                    $wifiprobedata->essid6=$essid6;
                }

                if(array_key_exists("essid7",$detail_data[$i]))
                {
                    $essid7 = $detail_data[$i]["essid7"];
                    $wifiprobedata->essid7=$essid7;
                }

                $wifiprobedata->save();

            }


        }
        catch (Exception $e)
        {
            info($e->getLine().': '.$e->getMessage().'-----'.$e->getCode());
        }



    }
}
