<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WifiProbeData extends Model
{
    protected $table = 'wifiprobetest_data';

    protected $fillable = ['probeid','mmac','rssi','router','ts','tmc','tc','ds','essid1','essid2','essid3','essid4','essid5','essid6','essid7'];
}
